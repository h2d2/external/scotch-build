# -*- coding: UTF-8 -*-

import MUC_SCons
import re
import os

def get_ver(env):
    ver = env.Dir('#').abspath
    ver = os.path.basename(ver)     # scotch_5.1.7_esmumps
    m = re.match("scotch_(?P<vmaj>[0-9]+)\.(?P<vmin>[0-9]+)\.(?P<vrev>[0-9]+)", ver)
    return [ m.group('vmaj'), m.group('vmin'), m.group('vrev') ]

def get_dfn(env):
    ver = get_ver(env)
    if ver < ['6', '0', '6']:
        dfn = [ 'SCOTCH_VERSION=%s' % ver[0], 'SCOTCH_RELEASE=%s' % ver[1], 'SCOTCH_PATCHLEVEL=%s' % ver[2] ]
    else:
        dfn = [ 'SCOTCH_VERSION_NUM=%s' % ver[0], 'SCOTCH_RELEASE_NUM=%s' % ver[1], 'SCOTCH_PATCHLEVEL_NUM=%s' % ver[2] ]
    if (env['fintegersize'] == '8'):
        dfn += [ 'INTSIZE64', 'IDXSIZE64' ]
    return dfn

def extractDeps(env, tag):
    scrptDir = env.Dir('.').abspath
    makeFile = os.path.join(scrptDir, 'Makefile')
    doAcc = False
    objs = []
    for l in open(makeFile):
        l = l.strip()
        if (l.split('=')[0].strip() == tag): 
            doAcc = True
            l = l.split('=')[1].strip()
        elif not l: 
            doAcc = False
        if doAcc:
            l = l.split('\\')[0]
            l = l.strip()
            l = (' ' + l).replace(' parser_', ' last_resort/parser_')
            l = l.strip()
            l = l.replace('$(OBJ)', '.c')
            objs.append(l)
    return objs

