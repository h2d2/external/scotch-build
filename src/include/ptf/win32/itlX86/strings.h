#ifndef SCOTCH_WINDOWS_STRINGS_H
#define SCOTCH_WINDOWS_STRINGS_H

#ifndef strcasecmp
#  define strcasecmp  stricmp
#endif

#ifndef strncasecmp
#  define strncasecmp strnicmp
#endif

#endif // SCOTCH_WINDOWS_STRINGS_H
