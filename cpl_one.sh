#!/bin/bash
[ -z "$TMP" ] && echo 'TMP must be defined' && exit
[ -z "$INRS_LXT" ] && echo 'INRS_LXT must be defined' && exit
[ -z "$INRS_DEV" ] && echo 'INRS_DEV must be defined' && exit
[ -z "$INRS_BLD" ] && echo 'INRS_BLD must be defined' && exit

LCL_CPL=$1 && shift
LCL_MPI=$1 && shift
LCL_ISZ=$1 && shift
if [ -n $LCL_ISZ ] && [ "$LCL_ISZ" = "i8" ]; then
   LCL_ISZ='fintegersize=8'
else
   LCL_ISZ=''
fi

SCONSTRUCT=$INRS_BLD/MUC_SCons/SConstruct
scons -Q --debug=explain --file=$SCONSTRUCT compilers=$LCL_CPL mpilibs=$LCL_MPI  $LCL_ISZ  docpplink=on  builds=debug
scons -Q --debug=explain --file=$SCONSTRUCT compilers=$LCL_CPL mpilibs=$LCL_MPI  $LCL_ISZ  docpplink=on  builds=release
scons -Q --debug=explain --file=$SCONSTRUCT compilers=$LCL_CPL mpilibs=$LCL_MPI  $LCL_ISZ  docpplink=on  builds=debug   forcestatic=on
scons -Q --debug=explain --file=$SCONSTRUCT compilers=$LCL_CPL mpilibs=$LCL_MPI  $LCL_ISZ  docpplink=on  builds=release forcestatic=on

